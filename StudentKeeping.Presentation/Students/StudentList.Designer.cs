﻿using StudentKeeping.Presentation.test;

namespace StudentKeeping.Presentation.Students
{
    partial class StudentList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsDeletedHide = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Vardas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pavardė = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TelNr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Adresas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Atnaujinti = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Ištrinti = new System.Windows.Forms.DataGridViewButtonColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.checkRodytiVisus = new System.Windows.Forms.CheckBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.dataGridViewButtonColumn1 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewButtonColumn2 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtAdresas = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTelNr = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPavardė = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtVardas = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.IsDeletedHide,
            this.Vardas,
            this.Pavardė,
            this.TelNr,
            this.Adresas,
            this.Atnaujinti,
            this.Ištrinti});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataGridView1.Location = new System.Drawing.Point(0, 121);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(653, 270);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // Id
            // 
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // IsDeletedHide
            // 
            this.IsDeletedHide.HeaderText = "IsDeletedHide";
            this.IsDeletedHide.Name = "IsDeletedHide";
            this.IsDeletedHide.ReadOnly = true;
            this.IsDeletedHide.Visible = false;
            // 
            // Vardas
            // 
            this.Vardas.HeaderText = "Vardas";
            this.Vardas.Name = "Vardas";
            this.Vardas.ReadOnly = true;
            // 
            // Pavardė
            // 
            this.Pavardė.HeaderText = "Pavardė";
            this.Pavardė.Name = "Pavardė";
            this.Pavardė.ReadOnly = true;
            // 
            // TelNr
            // 
            this.TelNr.HeaderText = "Tel. Nr.";
            this.TelNr.Name = "TelNr";
            this.TelNr.ReadOnly = true;
            // 
            // Adresas
            // 
            this.Adresas.HeaderText = "Adresas";
            this.Adresas.Name = "Adresas";
            this.Adresas.ReadOnly = true;
            this.Adresas.Width = 150;
            // 
            // Atnaujinti
            // 
            this.Atnaujinti.HeaderText = "";
            this.Atnaujinti.Name = "Atnaujinti";
            this.Atnaujinti.ReadOnly = true;
            this.Atnaujinti.Text = "Atnaujinti";
            this.Atnaujinti.UseColumnTextForButtonValue = true;
            this.Atnaujinti.Width = 80;
            // 
            // Ištrinti
            // 
            this.Ištrinti.HeaderText = "";
            this.Ištrinti.Name = "Ištrinti";
            this.Ištrinti.ReadOnly = true;
            this.Ištrinti.Text = "Ištrinti";
            this.Ištrinti.UseColumnTextForButtonValue = true;
            this.Ištrinti.Width = 80;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(518, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(123, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Naujas Stundentas";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkRodytiVisus
            // 
            this.checkRodytiVisus.AutoSize = true;
            this.checkRodytiVisus.Location = new System.Drawing.Point(259, 68);
            this.checkRodytiVisus.Name = "checkRodytiVisus";
            this.checkRodytiVisus.Size = new System.Drawing.Size(94, 17);
            this.checkRodytiVisus.TabIndex = 2;
            this.checkRodytiVisus.Text = "Rodyti ištrintus";
            this.checkRodytiVisus.UseVisualStyleBackColor = true;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(359, 64);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 3;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // dataGridViewButtonColumn1
            // 
            this.dataGridViewButtonColumn1.HeaderText = "";
            this.dataGridViewButtonColumn1.Name = "dataGridViewButtonColumn1";
            this.dataGridViewButtonColumn1.Text = "Atnaujinti";
            this.dataGridViewButtonColumn1.UseColumnTextForButtonValue = true;
            this.dataGridViewButtonColumn1.Width = 80;
            // 
            // dataGridViewButtonColumn2
            // 
            this.dataGridViewButtonColumn2.HeaderText = "";
            this.dataGridViewButtonColumn2.Name = "dataGridViewButtonColumn2";
            this.dataGridViewButtonColumn2.Text = "Ištrinti";
            this.dataGridViewButtonColumn2.UseColumnTextForButtonValue = true;
            this.dataGridViewButtonColumn2.Width = 80;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtAdresas);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtTelNr);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtPavardė);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtVardas);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnRefresh);
            this.groupBox1.Controls.Add(this.checkRodytiVisus);
            this.groupBox1.Location = new System.Drawing.Point(13, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(440, 98);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filtras";
            // 
            // txtAdresas
            // 
            this.txtAdresas.Location = new System.Drawing.Point(232, 42);
            this.txtAdresas.Name = "txtAdresas";
            this.txtAdresas.Size = new System.Drawing.Size(100, 20);
            this.txtAdresas.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(179, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Adresas";
            // 
            // txtTelNr
            // 
            this.txtTelNr.Location = new System.Drawing.Point(232, 13);
            this.txtTelNr.Name = "txtTelNr";
            this.txtTelNr.Size = new System.Drawing.Size(100, 20);
            this.txtTelNr.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(184, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Tel. Nr.";
            // 
            // txtPavardė
            // 
            this.txtPavardė.Location = new System.Drawing.Point(61, 42);
            this.txtPavardė.Name = "txtPavardė";
            this.txtPavardė.Size = new System.Drawing.Size(100, 20);
            this.txtPavardė.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Pavardė";
            // 
            // txtVardas
            // 
            this.txtVardas.Location = new System.Drawing.Point(61, 16);
            this.txtVardas.Name = "txtVardas";
            this.txtVardas.Size = new System.Drawing.Size(100, 20);
            this.txtVardas.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Vardas";
            // 
            // StudentList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(653, 391);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "StudentList";
            this.Text = "Student List";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox checkRodytiVisus;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsDeletedHide;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vardas;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pavardė;
        private System.Windows.Forms.DataGridViewTextBoxColumn TelNr;
        private System.Windows.Forms.DataGridViewTextBoxColumn Adresas;
        private System.Windows.Forms.DataGridViewButtonColumn Atnaujinti;
        private System.Windows.Forms.DataGridViewButtonColumn Ištrinti;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewButtonColumn1;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewButtonColumn2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtAdresas;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTelNr;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPavardė;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtVardas;
        private System.Windows.Forms.Label label1;
    }
}