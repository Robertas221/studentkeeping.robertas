﻿namespace StudentKeeping.Presentation.Students
{
    partial class SaveStudent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtStudentoId = new System.Windows.Forms.TextBox();
            this.txtVardas = new System.Windows.Forms.TextBox();
            this.txtPavardė = new System.Windows.Forms.TextBox();
            this.txtTelNr = new System.Windows.Forms.TextBox();
            this.txtAdresas = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSaugoti = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtStudentoId
            // 
            this.txtStudentoId.Location = new System.Drawing.Point(3, 3);
            this.txtStudentoId.Name = "txtStudentoId";
            this.txtStudentoId.Size = new System.Drawing.Size(15, 20);
            this.txtStudentoId.TabIndex = 0;
            this.txtStudentoId.Visible = false;
            // 
            // txtVardas
            // 
            this.txtVardas.Location = new System.Drawing.Point(68, 32);
            this.txtVardas.Name = "txtVardas";
            this.txtVardas.Size = new System.Drawing.Size(100, 20);
            this.txtVardas.TabIndex = 1;
            // 
            // txtPavardė
            // 
            this.txtPavardė.Location = new System.Drawing.Point(68, 58);
            this.txtPavardė.Name = "txtPavardė";
            this.txtPavardė.Size = new System.Drawing.Size(100, 20);
            this.txtPavardė.TabIndex = 2;
            // 
            // txtTelNr
            // 
            this.txtTelNr.Location = new System.Drawing.Point(68, 84);
            this.txtTelNr.Name = "txtTelNr";
            this.txtTelNr.Size = new System.Drawing.Size(100, 20);
            this.txtTelNr.TabIndex = 3;
            // 
            // txtAdresas
            // 
            this.txtAdresas.Location = new System.Drawing.Point(68, 110);
            this.txtAdresas.Name = "txtAdresas";
            this.txtAdresas.Size = new System.Drawing.Size(100, 20);
            this.txtAdresas.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Vardas:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Pavardė:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Tel. Nr.:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Adresas:";
            // 
            // btnSaugoti
            // 
            this.btnSaugoti.Location = new System.Drawing.Point(68, 136);
            this.btnSaugoti.Name = "btnSaugoti";
            this.btnSaugoti.Size = new System.Drawing.Size(75, 23);
            this.btnSaugoti.TabIndex = 9;
            this.btnSaugoti.Text = "Saugoti";
            this.btnSaugoti.UseVisualStyleBackColor = true;
            this.btnSaugoti.Click += new System.EventHandler(this.btnSaugoti_Click);
            // 
            // SaveStudent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(226, 195);
            this.Controls.Add(this.btnSaugoti);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtAdresas);
            this.Controls.Add(this.txtTelNr);
            this.Controls.Add(this.txtPavardė);
            this.Controls.Add(this.txtVardas);
            this.Controls.Add(this.txtStudentoId);
            this.Name = "SaveStudent";
            this.Text = "SaveStudent";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtStudentoId;
        private System.Windows.Forms.TextBox txtVardas;
        private System.Windows.Forms.TextBox txtPavardė;
        private System.Windows.Forms.TextBox txtTelNr;
        private System.Windows.Forms.TextBox txtAdresas;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSaugoti;
    }
}