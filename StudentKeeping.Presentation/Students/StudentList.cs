﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StudentKeeping.Services;
using StudentKeeping.Dal.GridųFiltrai;
using StudentKeeping.Presentation.test;

namespace StudentKeeping.Presentation.Students
{
    public partial class StudentList : Form
    {
        private readonly StudentsService _service;
        public StudentList()
        {
            InitializeComponent();
            _service = new StudentsService();
            UžpildykGridą(new StudentųGridFiltas());
        }

        private void UžpildykGridą(StudentųGridFiltas gridFiltras)
        {
            dataGridView1.Rows.Clear();
            var duomenys = _service.GaukStudentus(gridFiltras);
            foreach (DataRow studentas in duomenys.Rows)
            {
                var index = dataGridView1.Rows.Add();
                dataGridView1.Rows[index].Cells["Id"].Value = studentas["Id"];
                dataGridView1.Rows[index].Cells["Vardas"].Value = studentas["Vardas"];
                dataGridView1.Rows[index].Cells["Pavardė"].Value = studentas["Pavarde"];
                dataGridView1.Rows[index].Cells["Adresas"].Value = studentas["Adresas"];
                dataGridView1.Rows[index].Cells["TelNr"].Value = studentas["TelNr"];
                dataGridView1.Rows[index].Cells["IsDeletedHide"].Value = studentas["IsDeleted"];

                if (bool.Parse(studentas["IsDeleted"].ToString()))
                {
                    dataGridView1.Rows[index].DefaultCellStyle.BackColor = Color.Red;
                }
                
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 6)
            {
                var id = (int)dataGridView1.Rows[e.RowIndex].Cells["Id"].Value;
                SaveStudent saveStudent = new SaveStudent(id);
                saveStudent.FormClosed += SaveStudent_FormClosed;
                saveStudent.ShowDialog();
            }

            if (e.ColumnIndex == 7)
            {
                //Istrinti
                var isDeleted = bool.Parse(dataGridView1.Rows[e.RowIndex].Cells["IsDeletedHide"].Value.ToString());
                if (isDeleted)
                {
                    MessageBox.Show("Šis studentas jau ištrintas");
                } else
                {
                    if (MessageBox.Show("Ar tikrai norite ištrinti šį studentą?", "Ištrinti studentą", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        var id = (int)dataGridView1.Rows[e.RowIndex].Cells["Id"].Value;
                        _service.IšrinkStudentą(id);
                        UžpildykGridą(GaukStudentųGridFiltrą());
                    }
                }
            }
        }

        private void SaveStudent_FormClosed(object sender, FormClosedEventArgs e)
        {
            UžpildykGridą(GaukStudentųGridFiltrą());
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            UžpildykGridą(GaukStudentųGridFiltrą());
        }

        private StudentųGridFiltas GaukStudentųGridFiltrą()
        {
            return new StudentųGridFiltas
            {
                Vardas = txtVardas.Text,
                Pavardė = txtPavardė.Text,
                TelNr = txtTelNr.Text,
                Adresas = txtAdresas.Text,
                ArRodytiIrIštrintus = checkRodytiVisus.Checked
            };
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
