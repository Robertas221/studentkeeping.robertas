﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StudentKeeping.Services;
using StudentKeeping.Dal.GridųFiltrai;
using StudentKeeping.Dal.Models;

namespace StudentKeeping.Presentation.Mokytojai
{
    public partial class MokytojųSąrašas : Form
    {
        public int? MokytojoId { get; set; }
        public string MokytojoVardasPavardė { get; set; }

        private MokytojaiService _service;
        private DalykaiService _dalykaiService;
        public MokytojųSąrašas()
        {
            InitializeComponent();
            _service = new MokytojaiService();
            _dalykaiService = new DalykaiService();
            UžpildykGridą(new MokytojųGridFiltras());
            UžpildykDalykųSelectList();
        }

        private void UžpildykDalykųSelectList()
        {
            var dalykai = _dalykaiService.GaukDalykus();

            listBoxDalykai.DataSource = new BindingSource(dalykai, null);
            listBoxDalykai.DisplayMember = "Pavadinimas";
            listBoxDalykai.ValueMember = "Id";
        }

        public void UžpildykGridą(MokytojųGridFiltras filtras)
        {
            dataGridView1.Rows.Clear();
            var mokytojai = _service.GaukMokytojus(filtras);

            foreach (DataRow mokytojas in mokytojai.Rows)
            {
                var index = dataGridView1.Rows.Add();
                dataGridView1.Rows[index].Cells["Id"].Value = mokytojas["Id"];
                dataGridView1.Rows[index].Cells["Vardas"].Value = mokytojas["Vardas"];
                dataGridView1.Rows[index].Cells["Pavardė"].Value = mokytojas["Pavarde"];
                dataGridView1.Rows[index].Cells["Dalykas"].Value = mokytojas["Pavadinimas"];
                dataGridView1.Rows[index].Cells["DalykoId"].Value = mokytojas["DalykoId"];
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1 || e.ColumnIndex == 2)
            {
                MokytojoId = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
                var vardas = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
                var pavardė = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();

                MokytojoVardasPavardė = $"{vardas} {pavardė}";
                this.Close();
            }
        }

        private MokytojųGridFiltras GaukFiltrus()
        {
            var vardas = txtVardas.Text;
            var pavardė = txtPavardė.Text;
            var ids = new List<int>();

            foreach (var item in listBoxDalykai.SelectedItems)
            {
                var id = ((Dalykas)item).Id;
                ids.Add(id.Value);
            }

            return new MokytojųGridFiltras
            {
                Vardas = vardas,
                Pavardė = pavardė,
                MokytojųIds = ids
            };
        }

        private void btnIeškoti_Click(object sender, EventArgs e)
        {
            UžpildykGridą(GaukFiltrus());
        }
    }
}
