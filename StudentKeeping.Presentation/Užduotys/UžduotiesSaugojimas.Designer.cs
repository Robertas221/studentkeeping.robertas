﻿namespace StudentKeeping.Presentation.Užduotys
{
    partial class UžduotiesSaugojimas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbxDalykoId = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMokytojoVardasPavardė = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnPickMokytojas = new System.Windows.Forms.Button();
            this.txtMokytojoId = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtUžduotiesNuoroda = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnĮkelkFailą = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.datetimePateikimoData = new System.Windows.Forms.DateTimePicker();
            this.datetimePateikimoLaikas = new System.Windows.Forms.DateTimePicker();
            this.datetimeAtlikimoLaikas = new System.Windows.Forms.DateTimePicker();
            this.datetimeAtlikimoData = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.btnSaugoti = new System.Windows.Forms.Button();
            this.txtId = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // cmbxDalykoId
            // 
            this.cmbxDalykoId.FormattingEnabled = true;
            this.cmbxDalykoId.Location = new System.Drawing.Point(109, 34);
            this.cmbxDalykoId.Name = "cmbxDalykoId";
            this.cmbxDalykoId.Size = new System.Drawing.Size(121, 21);
            this.cmbxDalykoId.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(55, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Dalykas:";
            // 
            // txtMokytojoVardasPavardė
            // 
            this.txtMokytojoVardasPavardė.Location = new System.Drawing.Point(109, 62);
            this.txtMokytojoVardasPavardė.Name = "txtMokytojoVardasPavardė";
            this.txtMokytojoVardasPavardė.Size = new System.Drawing.Size(121, 20);
            this.txtMokytojoVardasPavardė.TabIndex = 2;
            this.txtMokytojoVardasPavardė.TextChanged += new System.EventHandler(this.txtMokytojoVardasPavardė_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(45, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Mokytojas:";
            // 
            // btnPickMokytojas
            // 
            this.btnPickMokytojas.Location = new System.Drawing.Point(236, 62);
            this.btnPickMokytojas.Name = "btnPickMokytojas";
            this.btnPickMokytojas.Size = new System.Drawing.Size(23, 19);
            this.btnPickMokytojas.TabIndex = 4;
            this.btnPickMokytojas.Text = "?";
            this.btnPickMokytojas.UseVisualStyleBackColor = true;
            this.btnPickMokytojas.Click += new System.EventHandler(this.btnPickMokytojas_Click);
            // 
            // txtMokytojoId
            // 
            this.txtMokytojoId.Location = new System.Drawing.Point(251, 12);
            this.txtMokytojoId.Name = "txtMokytojoId";
            this.txtMokytojoId.Size = new System.Drawing.Size(32, 20);
            this.txtMokytojoId.TabIndex = 5;
            this.txtMokytojoId.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(265, 62);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(23, 19);
            this.button1.TabIndex = 6;
            this.button1.Text = "X";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(109, 89);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(121, 20);
            this.textBox1.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Pavadinimas:";
            // 
            // txtUžduotiesNuoroda
            // 
            this.txtUžduotiesNuoroda.Location = new System.Drawing.Point(109, 116);
            this.txtUžduotiesNuoroda.Name = "txtUžduotiesNuoroda";
            this.txtUžduotiesNuoroda.Size = new System.Drawing.Size(121, 20);
            this.txtUžduotiesNuoroda.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 119);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Užduoties nuoroda:";
            // 
            // btnĮkelkFailą
            // 
            this.btnĮkelkFailą.Location = new System.Drawing.Point(236, 116);
            this.btnĮkelkFailą.Name = "btnĮkelkFailą";
            this.btnĮkelkFailą.Size = new System.Drawing.Size(30, 20);
            this.btnĮkelkFailą.TabIndex = 12;
            this.btnĮkelkFailą.Text = "...";
            this.btnĮkelkFailą.UseVisualStyleBackColor = true;
            this.btnĮkelkFailą.Click += new System.EventHandler(this.btnĮkelkFailą_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 147);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Pateikimo data:";
            // 
            // datetimePateikimoData
            // 
            this.datetimePateikimoData.CustomFormat = "yyyy-MM-dd";
            this.datetimePateikimoData.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datetimePateikimoData.Location = new System.Drawing.Point(110, 143);
            this.datetimePateikimoData.Name = "datetimePateikimoData";
            this.datetimePateikimoData.Size = new System.Drawing.Size(99, 20);
            this.datetimePateikimoData.TabIndex = 14;
            // 
            // datetimePateikimoLaikas
            // 
            this.datetimePateikimoLaikas.CustomFormat = "HH:mm";
            this.datetimePateikimoLaikas.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datetimePateikimoLaikas.Location = new System.Drawing.Point(215, 143);
            this.datetimePateikimoLaikas.Name = "datetimePateikimoLaikas";
            this.datetimePateikimoLaikas.ShowUpDown = true;
            this.datetimePateikimoLaikas.Size = new System.Drawing.Size(51, 20);
            this.datetimePateikimoLaikas.TabIndex = 15;
            // 
            // datetimeAtlikimoLaikas
            // 
            this.datetimeAtlikimoLaikas.CustomFormat = "HH:mm";
            this.datetimeAtlikimoLaikas.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datetimeAtlikimoLaikas.Location = new System.Drawing.Point(214, 169);
            this.datetimeAtlikimoLaikas.Name = "datetimeAtlikimoLaikas";
            this.datetimeAtlikimoLaikas.ShowUpDown = true;
            this.datetimeAtlikimoLaikas.Size = new System.Drawing.Size(51, 20);
            this.datetimeAtlikimoLaikas.TabIndex = 18;
            // 
            // datetimeAtlikimoData
            // 
            this.datetimeAtlikimoData.CustomFormat = "yyyy-MM-dd";
            this.datetimeAtlikimoData.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datetimeAtlikimoData.Location = new System.Drawing.Point(109, 169);
            this.datetimeAtlikimoData.Name = "datetimeAtlikimoData";
            this.datetimeAtlikimoData.Size = new System.Drawing.Size(99, 20);
            this.datetimeAtlikimoData.TabIndex = 17;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(34, 173);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Atlikimo data:";
            // 
            // btnSaugoti
            // 
            this.btnSaugoti.Location = new System.Drawing.Point(109, 195);
            this.btnSaugoti.Name = "btnSaugoti";
            this.btnSaugoti.Size = new System.Drawing.Size(75, 23);
            this.btnSaugoti.TabIndex = 19;
            this.btnSaugoti.Text = "Saugoti";
            this.btnSaugoti.UseVisualStyleBackColor = true;
            this.btnSaugoti.Click += new System.EventHandler(this.btnSaugoti_Click);
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(7, 12);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(43, 20);
            this.txtId.TabIndex = 20;
            this.txtId.Visible = false;
            // 
            // UžduotiesSaugojimas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(295, 236);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.btnSaugoti);
            this.Controls.Add(this.datetimeAtlikimoLaikas);
            this.Controls.Add(this.datetimeAtlikimoData);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.datetimePateikimoLaikas);
            this.Controls.Add(this.datetimePateikimoData);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnĮkelkFailą);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtUžduotiesNuoroda);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtMokytojoId);
            this.Controls.Add(this.btnPickMokytojas);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtMokytojoVardasPavardė);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbxDalykoId);
            this.Name = "UžduotiesSaugojimas";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbxDalykoId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtMokytojoVardasPavardė;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnPickMokytojas;
        private System.Windows.Forms.TextBox txtMokytojoId;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtUžduotiesNuoroda;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnĮkelkFailą;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker datetimePateikimoData;
        private System.Windows.Forms.DateTimePicker datetimePateikimoLaikas;
        private System.Windows.Forms.DateTimePicker datetimeAtlikimoLaikas;
        private System.Windows.Forms.DateTimePicker datetimeAtlikimoData;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnSaugoti;
        private System.Windows.Forms.TextBox txtId;
    }
}