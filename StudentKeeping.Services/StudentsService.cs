﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using StudentKeeping.Dal;
using StudentKeeping.Dal.Models;
using StudentKeeping.Dal.GridųFiltrai;

namespace StudentKeeping.Services
{
    public class StudentsService : DBConnection
    {
        public StudentsService()
            : base("StudentKeepingConnectionString")
        {}

        public Studentas GaukStudentąPagalId(int id)
        {
            Studentas studentas = null;
            var gaukStudentąPagalIdSql = "SELECT * FROM Studentai WHERE Id = @Id";


            try
            {
                var gaukStudentąPagalIdCommand = new MySqlCommand(gaukStudentąPagalIdSql, Connection);
                gaukStudentąPagalIdCommand.Parameters.AddWithValue("@Id", id);

                Connection.Open();
                var studentoExecuteReader = gaukStudentąPagalIdCommand.ExecuteReader();

                while (studentoExecuteReader.Read())
                {
                    studentas = new Studentas();
                    studentas.Id = studentoExecuteReader.GetInt32(0);
                    studentas.Vardas = studentoExecuteReader.GetString(1);
                    studentas.Pavardė = studentoExecuteReader.GetString(2);
                    studentas.TelNr = !studentoExecuteReader.IsDBNull(3) ? studentoExecuteReader.GetString(3) : null;
                    studentas.Adresas = !studentoExecuteReader.IsDBNull(4) ? studentoExecuteReader.GetString(4) : null;
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Connection.Close();
            }


            return studentas;
        }

        public void SukurkNaują(Studentas studentas)
        {
            var sukurkNaująStudentąSql = @"INSERT INTO Studentai (Vardas, Pavarde, TelNr, Adresas)
                                           VALUES (@vardas, @pavarde, @telnr, @adresas)";

            var sukurkNaująStudentąСommand = new MySqlCommand(sukurkNaująStudentąSql, Connection);
            sukurkNaująStudentąСommand.Parameters.AddWithValue("@vardas", studentas.Vardas);
            sukurkNaująStudentąСommand.Parameters.AddWithValue("@pavarde", studentas.Pavardė);
            sukurkNaująStudentąСommand.Parameters.AddWithValue("@telnr", studentas.TelNr);
            sukurkNaująStudentąСommand.Parameters.AddWithValue("@adresas", studentas.Adresas);

            try
            {
                Connection.Open();
                sukurkNaująStudentąСommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                //log exeption
            }
            finally
            {
                Connection.Close();
            }

        }

        public void Redaguok(Studentas studentas)
        {
            var redaguokStudentąSql = @"UPDATE Studentai SET Vardas = @vardas, Pavarde = @pavarde, TelNr = @telnr, Adresas = @adresas WHERE Id = @id;";

            var redaguokStudentąСommand = new MySqlCommand(redaguokStudentąSql, Connection);
            redaguokStudentąСommand.Parameters.AddWithValue("@vardas", studentas.Vardas);
            redaguokStudentąСommand.Parameters.AddWithValue("@pavarde", studentas.Pavardė);
            redaguokStudentąСommand.Parameters.AddWithValue("@telnr", studentas.TelNr);
            redaguokStudentąСommand.Parameters.AddWithValue("@adresas", studentas.Adresas);
            redaguokStudentąСommand.Parameters.AddWithValue("@id", studentas.Id);

            try
            {
                Connection.Open();
                redaguokStudentąСommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                //log exeption
            }
            finally
            {
                Connection.Close();
            }
        }

        public DataTable GaukStudentus(StudentųGridFiltas studentųGridFiltas)
        {
            var stundetuSarasasSql = @"SELECT * FROM Studentai
                                       WHERE 
                                       (@IsDeleted IS NULL OR IsDeleted = @IsDeleted) AND
                                       (@Vardas IS NULL OR Vardas LIKE CONCAT('%', @Vardas, '%')) AND
                                       (@Pavardė IS NULL OR Pavarde LIKE CONCAT('%', @Pavardė, '%')) AND
                                       (@TelNr IS NULL OR TelNr LIKE CONCAT('%', @TelNr, '%')) AND
                                       (@Adresas IS NULL OR Adresas LIKE CONCAT('%', @Adresas, '%'));";

            var studentuSarasasCommand = new MySqlCommand(stundetuSarasasSql, Connection);
            studentuSarasasCommand.Parameters.AddWithValue("@IsDeleted", studentųGridFiltas.ArRodytiIrIštrintus ? (bool?)null : studentųGridFiltas.ArRodytiIrIštrintus);
            studentuSarasasCommand.Parameters.AddWithValue("@Vardas", !string.IsNullOrWhiteSpace(studentųGridFiltas.Vardas) ? studentųGridFiltas.Vardas : null);
            studentuSarasasCommand.Parameters.AddWithValue("@Pavardė", !string.IsNullOrWhiteSpace(studentųGridFiltas.Pavardė) ? studentųGridFiltas.Pavardė : null);
            studentuSarasasCommand.Parameters.AddWithValue("@TelNr", !string.IsNullOrWhiteSpace(studentųGridFiltas.TelNr) ? studentųGridFiltas.TelNr : null);
            studentuSarasasCommand.Parameters.AddWithValue("@Adresas", !string.IsNullOrWhiteSpace(studentųGridFiltas.Adresas) ? studentųGridFiltas.Adresas : null);

            MySqlDataAdapter studentuSarasasAdapter = new MySqlDataAdapter(studentuSarasasCommand);
            DataTable stundentuDataTable = new DataTable();
            studentuSarasasAdapter.Fill(stundentuDataTable);

            return stundentuDataTable;
        }

        public void IšrinkStudentą(int id)
        {
            var ištrintiStundentąSql = @"update studentai set IsDeleted = 1 where Id = @Id";
            var ištrintiStudentąCommand = new MySqlCommand(ištrintiStundentąSql, Connection);
            ištrintiStudentąCommand.Parameters.AddWithValue("@Id", id);

            Connection.Open();
            ištrintiStudentąCommand.ExecuteNonQuery();
            Connection.Close();
        }
    }
}
