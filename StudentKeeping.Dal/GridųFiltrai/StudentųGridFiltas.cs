﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentKeeping.Dal.GridųFiltrai
{
    public class StudentųGridFiltas
    {
        public string Vardas { get; set; }

        public string Pavardė { get; set; }

        public string TelNr { get; set; }

        public string Adresas { get; set; }

        public bool ArRodytiIrIštrintus { get; set; }
    }
}
