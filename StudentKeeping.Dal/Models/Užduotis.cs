﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentKeeping.Dal.Models
{
    public class Užduotis
    {
        public int? Id { get; set; }

        public int? DalykoId { get; set; }

        public int? MokytojoId { get; set; }

        public string MokytojoVardasPavardė { get; set; }

        public string Pavadinimas { get; set; }

        public string UžduotiesNuoroda { get; set; }

        public DateTime? ĮkelimoData { get; set; }

        public DateTime? PateikimoData { get; set; }

        public DateTime? IkiKadaAtlikti { get; set; }
    }
}
